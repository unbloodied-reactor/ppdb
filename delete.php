<?php
// include database connection file
include_once("koneksi.php");

// Check if the user id is provided in the URL parameter
if(isset($_GET['id'])) {
    $id = $_GET['id'];

    // Delete data from the database
    $deleteResult = mysqli_query($koneksi, "DELETE FROM tb_siswa WHERE id='$id'");

    // Check if delete query was successful
    if($deleteResult) {
        // Redirect to home_admin.php if successful
        header("Location: home_admin.php");
        exit();
    } else {
        // Display error message if delete query failed
        echo "Error deleting data: " . mysqli_error($koneksi);
    }
} else {
    // Redirect to home_admin.php if user id is not provided
    header("Location: home_admin.php");
}
?>
