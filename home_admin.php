<?php 
session_start();

include 'koneksi.php';

// Check if user is logged in
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header('location: login.php');
    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> PSB ONLINE | Administrator</title>
    <link rel="stylesheet" type="text/css" href="edmin.css">
</head>
<body>

    <!-- bagian header -->
    <header>
        <h1><a href="home_admin.php"></a>EDMIN PENDAFTARAN</h1>
        <ul>
           
            <li><a href="logout.php" class="logout">Keluar</a></li>
        </ul>
    </header>

    <!-- bagian content -->
    <section class="content">
        <h2 class="halo">Beranda</h2>
        <div class="box">           
            <h3>Selamat Datang di Menu Edmin</h3>
            <table class="table" border="1">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Nama</th>
                        <th>Tanggal Lahir</th>
                        <th>Jurusan</th>
                        <th>Kelamin</th>
                        <th>Agama</th>
                        <th>Alamat</th>
                    
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        $list_peserta = mysqli_query($koneksi, "SELECT * FROM tb_siswa");
                        while($row = mysqli_fetch_array($list_peserta)){
                    ?>
                    <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $row['email_kamu'] ?></td>
                        <td><?php echo $row['pw'] ?></td>
                        <td><?php echo $row['nama_kamu'] ?></td>
                        <td><?php echo $row['tanggal_lahir'] ?></td>
                        <td><?php echo $row['jurusan'] ?></td>
                        <td><?php echo $row['kelamin'] ?></td>
                        <td><?php echo $row['agama'] ?></td>
                        <td><?php echo $row['alamat'] ?></td>
                        <style>
    .hover-button a {
        display: inline-block;
        padding: 10px;
        background-color: #ccc;
        color: #000;
        text-decoration: none;
        transition: background-color 0.3s ease;
    }

    .hover-button a:hover {
        background-color: #999;
    }
</style>

<td class="hover-button">
    <a href="edit.php?id=<?php echo $row['id']; ?>">edit</a> |
    <a href="delete.php?id=<?php echo $row['id']; ?>">Delete</a>
</td>
                    </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>
    </section>
</body>
</html>
