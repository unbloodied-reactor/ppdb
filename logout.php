<?php
session_start();
// menghapus data session yang ada
unset($_SESSION ['username']);
unset($_SESSION ['password']);
unset($_SESSION ['nama_lengkap']);
unset($_SESSION ['level']);

session_destroy();

// redirect ke halaman index.php setelah logout
header('Location: login.php');
exit();
?>
