<?php
// include database connection file
include_once("koneksi.php");

// Check if the form is submitted
if(isset($_POST['submit'])) {
    // Get the form data
    $id = $_POST['id'];
    $email_kamu = $_POST['email_kamu'];
    $pw = $_POST['pw'];
    $nama_kamu = $_POST['nama_kamu'];
    $tanggal_lahir = $_POST['tanggal_lahir'];
    $jurusan = $_POST['jurusan'];
    $kelamin = $_POST['kelamin'];
    $agama = $_POST['agama'];
    $alamat = $_POST['alamat'];

    // Update data in the database
    $updateResult = mysqli_query($koneksi, "UPDATE tb_siswa SET email_kamu='$email_kamu', pw='$pw', nama_kamu='$nama_kamu', tanggal_lahir='$tanggal_lahir', jurusan='$jurusan', kelamin='$kelamin', agama='$agama', alamat='$alamat' WHERE id=$id");

    // Check if update query was successful
    if($updateResult) {
        // Redirect to home_admin.php if successful
        header("Location: home_admin.php");
        exit();
    } else {
        // Display error message if update query failed
        echo "Error updating data: " . mysqli_error($koneksi);
    }
} else {
    // Get the user id from URL parameter
    $id = $_GET['id'];

    $result = mysqli_query($koneksi, "SELECT * FROM tb_siswa WHERE id=$id");

    // Check if the user exists in the database
    if(mysqli_num_rows($result) > 0) {
        $data = mysqli_fetch_array($result);
        $email_kamu = $data['email_kamu'];
        $pw = $data['pw'];
        $nama_kamu = $data['nama_kamu'];
        $tanggal_lahir = $data['tanggal_lahir'];
        $jurusan = $data['jurusan'];
        $kelamin = $data['kelamin'];
        $agama = $data['agama'];
        $alamat = $data['alamat'];
    } else {
        // Redirect to home_admin.php if user does not exist
        header("Location: home_admin.php");
        exit();
    }
}
?>

<!-- Create the HTML form -->
<h2 style="padding-left: 50px;">EDIT DATA MURID BARU</h2> 
<link rel="stylesheet" type="text/css" href="style.css">

    <section class="box-formulir">
        
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
    <div class="box">
        <table border="0" class="table-form">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <tr>
                <td>EMAIL KAMU</td>
                <td>:</td>
                <td>
                    <input type="text" name="email_kamu" class="input_control" value="<?php echo $email_kamu; ?>">
                </td>
            </tr>
            <tr>
                <td>PASSWORD</td>
                <td>:</td>
                <td>
                    <input type="text" name="pw" class="input_control" value="<?php echo $pw; ?>">
                </td>
            </tr>
            <tr>
                <td>NAMA KAMU</td>
                <td>:</td>
                <td>
                    <input type="text" name="nama_kamu" class="input_control" value="<?php echo $nama_kamu; ?>">
                </td>
            </tr>
            <tr>
                <td>TANGGAL LAHIR</td>
                <td>:</td>
                <td>
                    <input type="text" name="tanggal_lahir" class="input_control" value="<?php echo $tanggal_lahir; ?>">
                </td>
            </tr>
            <tr>
                <td>JURUSAN</td>
                <td>:</td>
                <td>
                    <input type="text" name="jurusan" class="input_control" value="<?php echo $jurusan; ?>">
                </td>
            </tr>
            <tr>
                <td>KELAMIN</td>
                <td>:</td>
                <td>
                    <input type="text" name="kelamin" class="input_control" value="<?php echo $kelamin; ?>">
                </td>
            </tr>
            <tr>
                <td>AGAMA</td>
                <td>:</td>
                <td>
                    <input type="text" name="agama" class="input_control" value="<?php echo $agama; ?>">
                </td>
            </tr>
            <tr>
                <td>ALAMAT</td>
                <td>:</td>
                <td>
                    <input type="text" name="alamat" class="input_control" value="<?php echo $alamat; ?>">
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                <style>
    .hover-button input[type="submit"] {
        padding: 10px;
        background-color: cyan;
        color: none;
        border: none;
        cursor: pointer;
        transition: background-color 0.3s ease;
    }

    .hover-button input[type="submit"]:hover {
        background-color: #999;
    }
</style>

<td class="hover-button">
    <input type="submit" class="button" name="submit" value="Update">
</td>

                </td>
            </tr>
        </table>
    </div>
</form>


<style>
    .button-container {
        text-align: center;
    }

    .button {
        display: inline-block;
        margin-left: 50px;
        padding: 10px 20px;
        background-color: #ccc;
        color: #000;
        text-decoration: none;
        border: none;
        cursor: pointer;
        transition: background-color 0.3s ease;
    }

    .button:hover {
        background-color: #999;
    }
</style>

<div class="button-container">
    <a href="home_admin.php" class="button">Back to Home</a>
</div>
