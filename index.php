<?php 

session_start();

  include  'koneksi.php';
 
 
    if (isset($_POST['submit'])) {
    // id pendaftaran 
    $getMaxid = mysqli_query($koneksi, "SELECT MAX(RIGHT(id, 5)) AS id FROM tb_siswa");
    $d = mysqli_fetch_assoc($getMaxid);
    $generateid = 'P' .date('y').sprintf("%05s", $d['id'] + 1);

    // proses insert
    $insert = mysqli_query($koneksi, "INSERT INTO tb_siswa VALUES (
        '".$generateid."',
        '".$_POST['email_kamu']."',
        '".$_POST['pw']."',
        '".$_POST['nama_kamu']."',
        '".$_POST['tanggal_lahir']."',
        '".$_POST['jurusan']."',
        '".$_POST['kelamin']."',
        '".$_POST['agama']."',
        '".$_POST['alamat']."'
    )");

    if($insert){
        echo 'Data berhasil disimpan.';
    }else{
        echo 'Gagal menyimpan data: '.mysqli_error($koneksi);
    }
}

   
   
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
</head>
<body> 

   <!--  box formulir -->
   <section class="box-formulir">
  

     <h2 style="padding-left: 40px;"> PENDAFTARAN MURID BARU </h2> 
      <!-- bagian form -->
      <form action="" method="post">

           <div class="box" >
            <table border="0" class="table-form">
                <tr>
                    <td>EMAIL KAMU</td>
                    <td>:</td>
                    <td>
                        <input type="text" name="email_kamu" class="input_control">

                    </td>
                    <tr>
                        <td>PASSWORD</td>
                            <td>:</td>
                                       <td>
                                             <input type="text" name="pw"  class="input_control">

                                     </td>
                    </tr>
                    

                    <tr>
                        <td>NAMA KAMU</td>
                            <td>:</td>
                                       <td>
                                             <input type="text" name="nama_kamu"  class="input_control">

                                     </td>
                    </tr>
                    <tr>
                        <td>TANGGAL LAHIR</td>
                            <td>:</td>
                                       <td>
                                             <input type="date" name="tanggal_lahir"  class="input_control">
                                     </td>
                    </tr>
                    <tr>
                        <td>JURUSAN</td>
                            <td>:</td>
                                       <td>
                                             <input type="text" name="jurusan"  class="input_control">
                                     </td>
                    </tr>
                    <tr>
                        <td>KELAMIN</td>
                            <td>:</td>
                                       <td>
                                         <input type="radio" name="kelamin" value="laki_laki">laki_laki &nbsp;&nbsp;&nbsp;
                                         <input type="radio" name="kelamin" value="perempuan">perempuan
                                     </td>
                    </tr>
                    <tr>
                        <td>AGAMA</td>
                            <td>:</td>
                                       <td>
                                             <input type="text" name="agama"  class="input_control">
                                     </td>
                    </tr>
                    <tr>
                        <td>ALAMAT</td>
                            <td>:</td>
                                       <td>
                                             <input type="text" name="alamat"  class="input_control">
                                     </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                              <td>
                                 <input type="submit" name="submit"  value="daftar" class="btn">
                              </td>
                             
                    </tr>

                 <a href="login.php" class="logout">Login</a></li>  
            </table>
           </div>


      </form>

         
   </section>


    
</body>
</html>
