<?php 
session_start();

include 'koneksi.php';

// Check if user is logged in
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header('location: login.php');
    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> PSB ONLINE | Administrator</title>
    <link rel="stylesheet" type="text/css" href="edmin.css">
</head>
<body>

    <!-- bagian header -->
    <header>
        <h1><a href="home_admin.php"></a></h1>
        <ul>
            <li><a href="logout.php" class="logout">Keluar</a></li>
        </ul>
    </header>

    <!-- bagian content -->
    <section class="content">
        <h2>Berikut nama siswa yang diterima di SMK Canda Bhirawa Pare,Jika nama anda ada di tabel silahkan datang ke sekolah pada hari senin 02 Desember 2023 </h2>
        <div class="box">
            <table class="table" border="1">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Tanggal Lahir</th>
                        <th>Jurusan</th>
                        <th>Kelamin</th>
                        <th>Alamat</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        $list_peserta = mysqli_query($koneksi, "SELECT * FROM tb_siswa");
                        while($row = mysqli_fetch_array($list_peserta)){
                    ?>
                    <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $row['nama_kamu'] ?></td>
                        <td><?php echo $row['tanggal_lahir'] ?></td>
                        <td><?php echo $row['jurusan'] ?></td>
                        <td><?php echo $row['kelamin'] ?></td>
                        <td><?php echo $row['alamat'] ?></td>                           
                    </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>
    </section>
</body>
</html>